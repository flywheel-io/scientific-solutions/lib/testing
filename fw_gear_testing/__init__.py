"""Gear testing suite and utilities"""

import logging

logging.basicConfig(level=logging.DEBUG)

from .files import *
from .gears import *
from .hierarchy import *
from .sdk import *
from .utils import *
